package game;

import abstracts.State;
import interfaces.ISearchable;
import interfaces.ISolution;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by maorshabo on 20/05/2018.
 */

public class PipeGame implements ISearchable {
  private PipeGameBoard board;

  public PipeGame(int rows, int columns) {
    this.board = new PipeGameBoard(rows, columns);
  }

  public PipeGame(String[][] board, int[] startPosition, int[] endPosition) {
    this.board = new PipeGameBoard(board, startPosition, endPosition);
  }

  public PipeGameBoard getBoard() {
    return board;
  }

  @Override
  public State getInitialState() {
    return new PipeGameState(this.getBoard());
  }

  @Override
  public State getGoalState() {
    return null;
  }

  @Override
  public boolean isGoal(State s) {
    PipeGameBoard board = (PipeGameBoard) s.getState();
    int[] startPosition = board.getStartPosition();
    // reset visited
    board.resetVisited();
    return this.traverseBoard(startPosition[0], startPosition[1], board);
  }

  private boolean traverseBoard(int i, int j, PipeGameBoard board) {
    Pipe p = board.getGrid().getCellAt(i, j);
    if (p.getIsEnd()) {
      return true;
    }
    if (p.getIsVisited() || !p.isInBounds(board.getRows(), board.getColumns()) || !p.isFlow(board.getNeighboursPossibleMoves(i, j)) /*|| p.getIsStart()*/) {
      return false;
    }
    // if no where to go
    if (p._possibleMoves.size() == 0) {
      return false;
    }
    p.setVisited(true);
    boolean[] traverseResults = new boolean[p._possibleMoves.size()];
    for (int idx = 0; idx < p._possibleMoves.size(); idx++) {
      traverseResults[idx] = traverseBoard(p._possibleMoves.get(idx)[0], p._possibleMoves.get(idx)[1], board);
    }
    return this.isTrue(traverseResults);
  }

  private boolean isTrue(boolean[] boolArray) {
    for (int i = 0; i < boolArray.length; i++) {
      if (boolArray[i]) return true;
    }
    return false;
  }

  @Override
  public ArrayList<State> getAllPossibleStates(State s) {
    return this.board.getAllPossibleStates(s);
  }

  @Override
  public Comparator<State> getComparator() {
    return new PipeGame.StateComparator();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!PipeGameBoard.class.isAssignableFrom(obj.getClass())) {
      return false;
    }
    final PipeGameBoard other = (PipeGameBoard) obj;
    return true;
  }

  class StateComparator implements Comparator<State> {
    /**
     * This function will calculate which of the given states are closer to the goal
     *
     * @param state1: the first state to compare
     * @param state2: the second state to compare
     * @return : case state2 is closer return -1. case state1 is closer return 1. case of no difference return 0.
     */
    @Override
    public int compare(State state1, State state2) {
      // In case the the current position of the given state is the same, compare by rotations
      // compare states by flow count
      int rotations1 = state1.getStep() != null ? ((PipeGameStep)state1.getStep()).getRotations() : 0;
      int rotations2 = state2.getStep() != null ? ((PipeGameStep)state2.getStep()).getRotations() : 0;

      /*double totalFlow1 = state1.getFlowCount() + rotations1;
      double totalFlow2 = state2.getFlowCount() + rotations2;

      if (totalFlow1 > totalFlow2) return -1;
      else if (totalFlow1 < totalFlow2) return 1;
      return Double.compare(state1.getCost(), state2.getCost());*/

      double cost1 = state1.generateCost();
      double cost2 = state2.generateCost();

      if (cost1 < cost2) return -1;
      else if (cost2 < cost1) return 1;
      else {
        if (state1.getFlowCount() > state2.getFlowCount()) return -1;
        else if (state1.getFlowCount() < state2.getFlowCount()) return 1;
        return Double.compare(rotations2, rotations1);
      }
    }
  }
}
