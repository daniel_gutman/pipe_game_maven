package game;

import java.util.Arrays;

/**
 * Created by maorshabo on 21/05/2018.
 */

public class EndPipe extends Pipe {
  public static final String END = "g";

  public EndPipe(int i, int j, Grid g) {
    super(Arrays.asList(END), i, j, false, true);
    this.generatePossibleMoves(i, j);
//    this.validatePossibleMoves(g);
  }

  public EndPipe(Pipe p, Grid g) {
    super(p);
    this.generatePossibleMoves(p.i, p.j);
//    this.validatePossibleMoves(g);
  }

  @Override
  public void generatePossibleMoves(int i, int j) {
    switch (this._symbol) {
      case END:
        this._possibleMoves = Arrays.asList(new int[]{i, j - 1}, new int[]{i - 1, j}, new int[]{i, j + 1}, new int[]{i + 1, j});
        break;
    }
  }
}
