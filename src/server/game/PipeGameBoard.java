package game;

import abstracts.State;
import abstracts.Step;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by maorshabo on 20/05/2018.
 */
public class PipeGameBoard implements Serializable {

  private Grid b;
  private int rows;
  private int columns;
  private int[] startPosition;
  private int[] endPosition;
  private int angularPipeCount;
  private int straightPipeCount;

  PipeGameBoard(int rows, int columns) {
    this.rows = rows;
    this.columns = columns;
    this.b = new Grid(rows, columns);
    this.startPosition = new int[]{0, 0};
    this.endPosition = new int[]{rows - 1, columns - 1};
    this.angularPipeCount = this.getPipeCount(AngularPipe.class);
    this.straightPipeCount = this.getPipeCount(StraightPipe.class);
//    System.out.print(this.printBoard());
  }

  PipeGameBoard(String[][] board, int[] startPosition, int[] endPosition) {
    this.rows = board.length;
    this.columns = board[0].length;
    this.b = new Grid(board);
    this.startPosition = startPosition;
    this.endPosition = endPosition;
    this.angularPipeCount = this.getPipeCount(AngularPipe.class);
    this.straightPipeCount = this.getPipeCount(StraightPipe.class);
//    System.out.print(this.printBoard());
  }

  PipeGameBoard(Grid g, PipeGameBoard board) {
    this.b = new Grid(g);
    this.rows = g.getRows();
    this.columns = g.getColumns();
    this.startPosition = board.startPosition;
    this.endPosition = board.endPosition;
    this.angularPipeCount = this.getPipeCount(AngularPipe.class);
    this.straightPipeCount = this.getPipeCount(StraightPipe.class);
  }

  public Grid getGrid() {
    return b;
  }

  public ArrayList<State> getAllPossibleStates(State s) {
    ArrayList<State> possibleStates = new ArrayList<>();
    PipeGameBoard stateBoard = (PipeGameBoard)s.getState();
    Grid g = new Grid(stateBoard.getGrid());
    for (int i = 0; i < stateBoard.rows; i++) {
      for (int j = 0; j < stateBoard.columns; j++) {
        PipeGameBoard newBoard = new PipeGameBoard(new Grid(g), stateBoard);
        Step step = newBoard.getGrid().changeCell(i, j);
        if (step != null) {
          newBoard.getGrid().validatePossibleMoves(newBoard.startPosition, newBoard.endPosition);
//          System.out.println("-------- State --------");
//          System.out.print(newBoard.printBoard());
          State<PipeGameBoard> state = new PipeGameState(newBoard);
          state.setStep(step);
          state.setCameFrom(s);
          state.setCost(state.generateCost());
          possibleStates.add(state);
        }
        else {
          /*System.out.println("-------- State --------");
          System.out.print(newBoard.printBoard());*/
        }
      }
    }
    return possibleStates;
  }

  public String printBoard() {
    return this.b.toString();
  }

  @Override
  public String toString() {
    return this.b.toString();
  }

  public int[] getStartPosition() {
    return this.startPosition;
  }

  public int[] getEndPosition() {
    return this.endPosition;
  }

  public List<int[]> getNeighboursPossibleMoves(int i, int j) {
    List<int[]> myNeighbours = this.getGrid().getCellAt(i, j)._possibleMoves;
    List<int[]> myNeighboursPossibleMoves = new ArrayList<>();
    for (int[] cell : myNeighbours) {
      try {
        List<int[]> possibleMoves = this.getGrid().getCellAt(cell[0], cell[1])._possibleMoves;
        if (possibleMoves != null && !this.getGrid().getCellAt(cell[0], cell[1]).getIsVisited()) {
//        if (possibleMoves != null) {
          myNeighboursPossibleMoves.addAll(possibleMoves);
        }
      } catch (IndexOutOfBoundsException e) {
        System.out.println(e.getMessage());
      }
    }
    return myNeighboursPossibleMoves;
  }

  public boolean isFlow(int[] from, int[] to) {
    Pipe fromPipe = this.b.getCellAt(from[0], from[1]);
    Pipe toPipe = this.b.getCellAt(to[0], to[1]);
    // if they are appear in each other possible moves
    if (fromPipe.isFlow(toPipe._possibleMoves)) {
      return true;
    }
    return false;
  }

  public void resetVisited() {
    this.b.resetVisited();
  }

  public int getRows() {
    return rows;
  }

  public int getColumns() {
    return columns;
  }

  public int getAngularPipeCount() {
    return angularPipeCount;
  }

  public int getStraightPipeCount() {
    return straightPipeCount;
  }

  private int getPipeCount(Class C) {
    int pipeCount = 0;
    for (int i = 0; i < this.rows; i++) {
      for (int j = 0; j < this.columns; j++) {
        if (C.getName().equals(this.b.getCellAt(i, j).getClass().getName())) {
          pipeCount++;
        }
      }
    }
    return pipeCount;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!PipeGameBoard.class.isAssignableFrom(obj.getClass())) {
      return false;
    }
    final PipeGameBoard other = (PipeGameBoard) obj;
    return this.b.equals(other.b);
  }
}
