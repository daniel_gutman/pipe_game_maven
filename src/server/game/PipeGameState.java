package game;

import abstracts.State;

import java.awt.*;

/**
 * Created by maorshabo on 20/05/2018.
 */
public class PipeGameState extends State {
  public PipeGameState(PipeGameBoard board) {
    super(board);
//    this._flowCount = this.flowCount(board.getStartPosition(), board, true) + this.flowCount(board.getEndPosition(), board, false);
    this._flowCount = this.flowCount(board.getStartPosition(), board, true);
  }

  private int flowCount(int[] cell, PipeGameBoard b, boolean isEnd) {
    Pipe p = b.getGrid().getCellAt(cell[0], cell[1]);
    if (p.getIsVisited() || (isEnd && p.getIsEnd() || (!isEnd && p.getIsStart())) || !p.isInBounds(b.getRows(), b.getColumns()) || p._possibleMoves.size() == 0) {
      return 0;
    }

    int[] flowResults = new int[]{0, 0};
    p.setVisited(true);
    for (int idx = 0; idx < p._possibleMoves.size(); idx++) {
      if (b.isFlow(cell, p._possibleMoves.get(idx))) {
        try {
          flowResults[idx] += flowCount(p._possibleMoves.get(idx), b, isEnd) + 1;
        } catch (ArrayIndexOutOfBoundsException e) {
        }
      }
    }
    return Math.max(flowResults[0], flowResults[1]);
  }

  @Override
  public double generateCost() {
    double cost = 0;
    try {
      PipeGameBoard currentBoard = (PipeGameBoard) this.state;
      PipeGameStep step = (PipeGameStep) this.step;
      int[] endPosition = currentBoard.getEndPosition();
      int[] currentPosition = step.getCell();
      // Calculate the absolute value of the way from current position to the goal
      cost = Math.abs(Point.distance(
              currentPosition[0],
              currentPosition[1],
              endPosition[0],
              endPosition[1]));
    } catch (Exception ex) {
      System.out.println(String.join(": ", "PipeGameState.generateCost(): Error details", ex.getMessage()));
    }
    return cost;
  }
}
