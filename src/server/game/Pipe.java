package game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by maorshabo on 21/05/2018.
 */

public abstract class Pipe implements Serializable {
  protected List<String> _possibleSymbols;
  protected List<int[]> _possibleMoves;
  private int _possibleRotations;
  private int _rotatedCount = 0;
  protected String _symbol;
  protected int i;
  protected int j;
  private boolean _isVisited;
  private boolean _isEnd;
  private boolean _isStart;

  Pipe(List<String> possibleSymbols, int i, int j, boolean isStart, boolean isEnd) {
    this._possibleSymbols = possibleSymbols;
    this._possibleRotations = possibleSymbols.size() - 1;
    if (isStart) {
      this._symbol = "s";
    }
    else if (isEnd) {
      this._symbol = "g";
    }
    else {
      this._symbol = this.getRandomSymbol();
    }
    this._isStart = isStart;
    this._isEnd = isEnd;
    this.i = i;
    this.j = j;
    this._isVisited = false;
  }

  Pipe(List<String> possibleSymbols, int i, int j, boolean isStart, boolean isEnd, String symbol) {
    this._possibleSymbols = possibleSymbols;
    this._possibleRotations = possibleSymbols.size() - 1;
    this._isStart = isStart;
    this._isEnd = isEnd;
    if (isStart) {
      this._symbol = "s";
    }
    else if (isEnd) {
      this._symbol = "g";
    }
    else {
      this._symbol = symbol;
    }
    this.i = i;
    this.j = j;
    this._isVisited = false;
  }

  Pipe(Pipe p) {
    this._possibleSymbols = p._possibleSymbols;
    this._possibleRotations = p._possibleRotations;
    this._symbol = p._symbol;
    this._isStart = p._isStart;
    this._isEnd = p._isEnd;
    this.i = p.i;
    this.j = p.j;
    this._rotatedCount = p._rotatedCount;
    this._isVisited = false;
  }

  private String getRandomSymbol() {
    Random rand = new Random();
    int n = rand.nextInt(_possibleSymbols.size());
    return this._possibleSymbols.get(n);
  }

  public boolean rotate() {
    if (this._rotatedCount < this._possibleRotations && !this._isEnd && !this._isStart) {
      int currentSymbolIdx = this._possibleSymbols.indexOf(this._symbol);
      this._symbol = _possibleSymbols.get((currentSymbolIdx + 1) % _possibleSymbols.size());
      this._rotatedCount++;
      return true;
    }
    return false;
  }

  @Override
  public String toString() {
    return this._symbol;
  }

  public abstract void generatePossibleMoves(int i, int j);

  public boolean isFlow(List<int[]> possibleMoves) {
    int[] myCell = {this.i, this.j};
    int appearanceCount = 0;
    if (possibleMoves == null) {
      return false;
    }
//    if (possibleMoves.indexOf(myCell) != possibleMoves.lastIndexOf(myCell)) return true;
    for (int[] cell : possibleMoves) {
      if (cell[0] == myCell[0] && cell[1] == myCell[1]) appearanceCount++;
    }
    return appearanceCount >= 1 || this._isStart || this._isEnd;
  }

  public boolean isInBounds(int rows, int columns) {
    if (this._possibleMoves == null) return false;
    for (int[] cell : this._possibleMoves) {
      if (cell[0] < 0 || cell[1] < 0 || cell[0] >= rows || cell[1] >= columns) {
        return false;
      }
    }
    return true;
  }

  public boolean getIsEnd() {
    return this._isEnd;
  }

  public boolean getIsStart() {
    return this._isStart;
  }

  public void validatePossibleMoves(Grid g) {
    ArrayList<int[]> newPossibleMoves = new ArrayList<>();
    for (int[] move : this._possibleMoves) {
      Pipe p = g.getCellAt(move[0], move[1]);
      if (p != null && p._possibleMoves != null) {
        for (int[] pMove : p._possibleMoves) {
          if (this.i == pMove[0] && this.j == pMove[1]) {
            newPossibleMoves.add(move);
          }
        }
      }
    }
    this._possibleMoves = newPossibleMoves;
  }

  public boolean getIsVisited() {
    return _isVisited;
  }

  public void setVisited(boolean _isVisited) {
    this._isVisited = _isVisited;
  }

  public int getRotatedCount() {
    return _rotatedCount;
  }
}
