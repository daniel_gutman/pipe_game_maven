package game;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by maorshabo on 21/05/2018.
 */

public class StartPipe extends Pipe {
  public static final String START = "s";

  public StartPipe(int i, int j, Grid g) {
    super(Arrays.asList(START), i, j, true, false);
    this.generatePossibleMoves(i, j);
  }

  public StartPipe(Pipe p, Grid g) {
    super(p);
    this.generatePossibleMoves(p.i, p.j);
  }

  @Override
  public void generatePossibleMoves(int i, int j) {
    switch (this._symbol) {
      case START:
        this._possibleMoves = Arrays.asList(new int[]{i, j - 1}, new int[]{i - 1, j}, new int[]{i, j + 1}, new int[]{i + 1, j});
        break;
    }
  }
}
