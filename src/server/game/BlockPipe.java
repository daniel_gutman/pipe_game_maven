package game;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by maorshabo on 21/05/2018.
 */

public class BlockPipe extends Pipe {
  public static final String BLOCK = " ";

  public BlockPipe(int i, int j) {
    super(Arrays.asList(BLOCK), i, j, false, false);
    this.generatePossibleMoves(i, j);
  }

  public BlockPipe(Pipe p) {
    super(p);
    this.generatePossibleMoves(p.i, p.j);
  }

  @Override
  public void generatePossibleMoves(int i, int j) {
    switch (this._symbol) {
      case BLOCK:
        this._possibleMoves = new ArrayList<>(0);
        break;
    }
  }
}
