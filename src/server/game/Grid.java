package game;

import abstracts.Step;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by maorshabo on 20/05/2018.
 */
public class Grid implements Serializable {
  private ArrayList<ArrayList<Pipe>> matrix;
  private int rows;
  private int columns;

  Grid(int rows, int columns) {
    this.matrix = new ArrayList<>(rows);
    this.rows = rows;
    this.columns = columns;
    for (int i = 0; i < rows; i++) {
      this.matrix.add(i, new ArrayList<>(rows));
      for (int j = 0; j < columns; j++) {
        this.matrix.get(i).add(j, this.getRandomPipe(i, j, rows - 1, columns - 1));
      }
    }
  }

  Grid(String[][] board) {
    this.matrix = new ArrayList<>(board.length);
    this.rows = board.length;
    this.columns = board[0].length;
    int[] startPosition = new int[0];
    int[] endPosition = new int[0];
    for (int i = 0; i < board.length; i++) {
      this.matrix.add(i, new ArrayList<>(board.length));
      for (int j = 0; j < board[i].length; j++) {
        boolean isStart = board[i][j].toLowerCase().equals("s");
        boolean isEnd = board[i][j].toLowerCase().equals("g");
        if (isEnd) {
          this.matrix.get(i).add(j, new EndPipe(i, j, this));
          endPosition = new int[]{i, j};
        }
        else if (isStart) {
          this.matrix.get(i).add(j, new StartPipe(i, j, this));
          startPosition = new int[]{i, j};
        }
        else if (board[i][j].equals(BlockPipe.BLOCK)) {
          this.matrix.get(i).add(j, new BlockPipe(i, j));
        }
        else if (board[i][j].equals(StraightPipe.LEFT_RIGHT) || board[i][j].equals(StraightPipe.TOP_BOTTOM)) {
          this.matrix.get(i).add(j, new StraightPipe(i, j, board[i][j]));
        } else {
          this.matrix.get(i).add(j, new AngularPipe(i, j, board[i][j]));
        }
      }
    }
    this.validatePossibleMoves(startPosition, endPosition);
  }

  Grid(Grid g) {
    this.matrix = new ArrayList<>(g.rows);
    this.rows = g.rows;
    this.columns = g.columns;
    for (int i = 0; i < g.rows; i++) {
      this.matrix.add(i, new ArrayList<>(g.columns));
      for (int j = 0; j < g.columns; j++) {
        Pipe p = g.getCellAt(i, j);
        if (p instanceof StartPipe) {
          this.matrix.get(i).add(j, new StartPipe(p, g));
        }
        else if (p instanceof EndPipe) {
          this.matrix.get(i).add(j, new EndPipe(p, g));
        }
        else if (p instanceof AngularPipe) {
          this.matrix.get(i).add(j, new AngularPipe(p));
        }
        else if (p instanceof StraightPipe) {
          this.matrix.get(i).add(j, new StraightPipe(p));
        }
        else if (p instanceof BlockPipe) {
          this.matrix.get(i).add(j, new BlockPipe(p));
        }
      }
    }
  }

  public void validatePossibleMoves(int[] startPosition, int[] endPosition) {
    StartPipe s = (StartPipe) this.getCellAt(startPosition[0], startPosition[1]);
    EndPipe e = (EndPipe) this.getCellAt(endPosition[0], endPosition[1]);
    s.validatePossibleMoves(this);
    e.validatePossibleMoves(this);
  }

  public int getRows() {
    return this.rows;
  }

  public int getColumns() {
    return this.columns;
  }

  private Pipe getRandomPipe(int i, int j, int rows, int columns) {
    Random rand = new Random();
    int n = rand.nextInt(2);
    boolean isStart = i == 0 && j == 0;
    boolean isEnd = rows == i && columns == j;
    if (isEnd) {
      return new EndPipe(i, j, this);
    }
    if (isStart) {
      return new StartPipe(i, j, this);
    }
    if (n == 1) {
      return new AngularPipe(i, j);
    } else {
      return new StraightPipe(i, j);
    }
  }

  public Pipe getCellAt(int i, int j) {
    try {
      return this.matrix.get(i).get(j);
    } catch (IndexOutOfBoundsException e) {
      return null;
    }
  }

  public void resetVisited() {
    for (int i = 0; i < this.matrix.size(); i++) {
      for (int j = 0; j < this.matrix.get(i).size(); j++) {
        Pipe p = this.matrix.get(i).get(j);
        p.setVisited(false);
      }
    }
  }

  public Step changeCell(Integer i, Integer j) {
    boolean rotateSuccess = this.matrix.get(i).get(j).rotate();
    if (rotateSuccess) {
      this.matrix.get(i).get(j).generatePossibleMoves(i, j);
//      Pipe cell = this.matrix.get(i).get(j);
//      return new PipeGameStep(i, j, cell.getRotatedCount());
      return new PipeGameStep(i, j, 1);
    }
    return null;
  }

  public String toString() {
//    return this.slimToString();
    String grid = this.drawLine();
    for (ArrayList<Pipe> row : this.matrix) {
      for (Pipe p : row) {
        grid = grid.concat(p.toString() + " | ");
      }
      grid = grid.concat("\n");
      grid = grid.concat(this.drawLine());
    }
    return grid;
  }

  private String slimToString() {
    String grid = "";
    for (ArrayList<Pipe> row : this.matrix) {
      for (Pipe p : row) {
        grid = grid.concat(p.toString());
      }
      grid = grid.concat("@@");
    }
    return grid;
  }

  private String drawLine() {
    String border = "─   ";
    String top = border;
    for (int i = 0; i < this.columns - 1; i++) {
      top = top.concat(border);
    }
    top = top.concat("\n");
    return top;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!Grid.class.isAssignableFrom(obj.getClass())) {
      return false;
    }
    final Grid other = (Grid) obj;
    return this.toString().equals(other.matrix.toString());
  }
}
