package game;

import abstracts.Step;

import java.util.Arrays;

/**
 * Created by maorshabo on 18/08/2018.
 */
public class PipeGameStep extends Step<int[]> {
  private int i;
  private int j;
  private int rotations;
  private int[] step;

  public PipeGameStep(int i, int j, int rotations) {
    this.i = i;
    this.j = j;
    this.rotations = rotations;
    this.step = new int[]{i, j ,rotations};
  }

  @Override
  public void setStep(int[] step) {
    this.step = step;
  }

  public void setRotations(int rotations) {
    this.rotations = rotations;
    this.step = new int[]{this.i, this.j ,rotations};
  }

  public int getRotations() {
    return this.rotations;
  }

  @Override
  public String toString() {
    return String.format("%d,%d,%d", this.step[0], this.step[1], this.step[2]);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!PipeGameStep.class.isAssignableFrom(obj.getClass())) {
      return false;
    }
    final PipeGameStep step = (PipeGameStep) obj;
    int[] stepCell = new int[]{step.i, step.j};
    int[] currentCell = this.getCell();
    return Arrays.equals(stepCell, currentCell);
  }

  public int[] getCell() {
    return new int[]{this.i, this.j};
  }

  public int getColumn() {
    return j;
  }

  public int getRow() {
    return i;
  }
}
