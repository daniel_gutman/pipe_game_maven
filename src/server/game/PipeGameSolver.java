package game;

import abstracts.State;
import common.Solution;
import interfaces.*;

import java.util.*;

/**
 * Created by maorshabo on 20/08/2018.
 */
public class PipeGameSolver implements ISolver {
  // This searcher is injected into the common solver and solves the given problem
  private ISearcher searcher;

  public PipeGameSolver(ISearcher searcher) {
    this.searcher = searcher;
  }

  public PipeGameSolution solve(ISearchable searchable) {
    return new PipeGameSolution(this.searcher.search(searchable));
  }

  private int[] get_position(String problem, String pos) {
    int[] position = new int[2];
    List<String> lines = Arrays.asList(problem.split(System.lineSeparator()));
    for (int lineIdx = 0; lineIdx < lines.size(); lineIdx++) {
      List<String> columns = Arrays.asList(lines.get(lineIdx).split(""));
      int colIdx = columns.indexOf(pos);
      if (colIdx > -1) {
        position[0] = lineIdx;
        position[1] = colIdx;
      }
    }
    return position;
  }

  private void handleSolution(ISolution solution) {
    if (solution != null && solution.getSteps() != null) {
      int n = this.searcher.getNumberOfNodesEvaluated();
      ArrayList<State> path = solution.getSteps();
      if (path.size() > 0) {
        PipeGameState s = (PipeGameState) path.get(path.size() - 1);
        PipeGameBoard b = (PipeGameBoard) s.getState();
        System.out.println("-------- GOAL STATE --------");
        System.out.print(b.toString());
      }
      System.out.println(n + " nodes evaluated using " + searcher.getName());
      System.out.print(solution.toString());
      System.out.println("-------------------------");
    } else {
      System.out.println("No Solution");
    }
  }

  private String[][] get_matrix_problem(String problem) {
    ArrayList<ArrayList<String>> matrix = new ArrayList<>();
    String[] lines = problem.split(System.lineSeparator());
    int lastLineLength = 0;
    for (int row = 0; row < lines.length; row++) {
      matrix.add(new ArrayList<String>());
      String[] chars = lines[row].split("");
      for (int col = 0; col < lines[row].length(); col++) {
        matrix.get(row).add(chars[col]);
      }
      if (row > 0) {
        lastLineLength = lines[row - 1].length();
        if (lastLineLength < lines[row].length()) {
          for (int k = 0; k < lines[row].length() - lastLineLength; k++) {
            matrix.get(row-1).add(" ");
          }
        }
        else {
          for (int k = 0; k < lastLineLength - lines[row].length(); k++) {
            matrix.get(row).add(" ");
          }
        }
      }
    }
    String[][] converted = new String[matrix.size()][];
    for (int i = 0; i < converted.length; i++)
      converted[i] = matrix.get(i).toArray(new String[matrix.get(i).size()]);
    return converted;
  }

  public ISolution solve(String problem) {
    int[] startPosition = this.get_position(problem, "s");
    int[] endPosition = this.get_position(problem, "g");

    if (Arrays.equals(startPosition, endPosition)) {
      System.out.println("start position and end position are equals");
      return null;
    }
    String[][] stringBoard = this.get_matrix_problem(problem);

    PipeGame p = new PipeGame(stringBoard, startPosition, endPosition);

    if (this.validateGame(p)) {
      this.searcher.reset();
      ISolution s = this.solve(p);
//      handleSolution(s);
      return s;
    }
    System.out.println("game is not valid");
    return null;
  }

  private boolean validateGame(PipeGame p) {
    int rowsCount = p.getBoard().getRows();
    int angularCount = p.getBoard().getAngularPipeCount();
    int straightCount = p.getBoard().getStraightPipeCount();
    if ((rowsCount == 1 && angularCount > 0) || (rowsCount > 1 && angularCount == 0 && straightCount > 0)/* || (straightCount == 0 && angularCount > 0)*/) {
      return false;
    }
    return true;
  }
}
