package game;

import abstracts.State;
import abstracts.Step;
import interfaces.ISolution;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by maorshabo on 17/06/2018.
 */
public class PipeGameSolution implements ISolution, Serializable {
  ArrayList<State> steps;

  PipeGameSolution(State goalState) {
    this.steps = this.backtrace(goalState);
  }

  PipeGameSolution(ISolution solution) {
    if (solution != null) {
      this.steps = solution.getSteps();
    }
    else {
      this.steps = null;
    }
  }

  private ArrayList<State> backtrace(State goalState) {
    ArrayList<State> tempPath = new ArrayList<>();
    State s = goalState;
    while (s.getCameFrom() != null) {
      tempPath.add(0, s);
      s = (State) s.getCameFrom();
    }
    return tempPath;
  }

  public ArrayList<State> getSteps() {
    return this.steps;
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    ArrayList<Step> mergedSteps = new ArrayList<Step>(0);
    if (this.steps != null) {
      if (this.steps.size() == 0) {
        return "No steps needed, the puzzle is already solved";
      }
      for (State state : this.steps) {
        PipeGameStep step = (PipeGameStep) state.getStep();
        int stepIdx = mergedSteps.indexOf(step);
        if (stepIdx > -1) {
          step.setRotations(step.getRotations() + 1);
          mergedSteps.set(stepIdx, step);
        } else {
          mergedSteps.add(state.getStep());
        }
      }
      for (Step step : mergedSteps) {
        result.append(step.toString()).append("\n");
      }
    }
    return result.toString();
  }
}
