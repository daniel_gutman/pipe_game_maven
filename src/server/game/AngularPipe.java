package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by maorshabo on 21/05/2018.
 */
public class AngularPipe extends Pipe {
  public static final String LEFT_BOTTOM = "7";
  public static final String RIGHT_BOTTOM = "F";
  public static final String LEFT_TOP = "J";
  public static final String RIGHT_TOP = "L";

  public AngularPipe(int i, int j) {
    super(Arrays.asList(LEFT_BOTTOM, LEFT_TOP, RIGHT_TOP, RIGHT_BOTTOM), i, j, false, false);
    this.generatePossibleMoves(i, j);
  }

  public AngularPipe(int i, int j, String symbol) {
    super(Arrays.asList(LEFT_BOTTOM, LEFT_TOP, RIGHT_TOP, RIGHT_BOTTOM), i, j, false, false, symbol);
    this.generatePossibleMoves(i, j);
  }

  public AngularPipe(Pipe p) {
    super(p);
    this.generatePossibleMoves(p.i, p.j);
  }

  public void generatePossibleMoves(int i, int j) {
    switch (this._symbol) {
      case LEFT_BOTTOM:
        this._possibleMoves = Arrays.asList(new int[]{i, j - 1}, new int[]{i + 1, j});
        break;
      case RIGHT_BOTTOM:
        this._possibleMoves = Arrays.asList(new int[]{i, j + 1}, new int[]{i + 1, j});
        break;
      case LEFT_TOP:
        this._possibleMoves = Arrays.asList(new int[]{i - 1, j}, new int[]{i, j - 1});
        break;
      case RIGHT_TOP:
        this._possibleMoves = Arrays.asList(new int[]{i - 1, j}, new int[]{i, j + 1});
        break;
    }
  }
}
