package game;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by maorshabo on 21/05/2018.
 */

public class StraightPipe extends Pipe {
  public static final String TOP_BOTTOM = "|";
  public static final String LEFT_RIGHT = "-";

  public StraightPipe(int i, int j) {
    super(Arrays.asList(TOP_BOTTOM, LEFT_RIGHT), i, j, false, false);
    this.generatePossibleMoves(i, j);
  }

  public StraightPipe(int i, int j, String symbol) {
    super(Arrays.asList(TOP_BOTTOM, LEFT_RIGHT), i, j, false, false, symbol);
    this.generatePossibleMoves(i, j);
  }

  public StraightPipe(Pipe p) {
    super(p);
    this.generatePossibleMoves(p.i, p.j);
  }

  @Override
  public void generatePossibleMoves(int i, int j) {
    switch (this._symbol) {
      case TOP_BOTTOM:
        this._possibleMoves = Arrays.asList(new int[]{i - 1, j}, new int[]{i + 1, j});
        break;
      case LEFT_RIGHT:
        this._possibleMoves = Arrays.asList(new int[]{i, j - 1}, new int[]{i, j + 1});
        break;
    }
  }
}
