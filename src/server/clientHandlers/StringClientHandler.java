package clientHandlers;

import game.PipeGameSolver;
import interfaces.*;

import java.io.*;

public class StringClientHandler implements IClientHandler {
  private ISolver solver;
  private ICacheManager cacheManager;

  public StringClientHandler(ICacheManager c, ISearcher searcher) {
    this.solver = new PipeGameSolver(searcher);
    this.cacheManager = c;
  }

  private BufferedReader reader;
  private PrintWriter writer;

  private String readRequest() {
    StringBuilder mat = new StringBuilder();
    String tmpLine;

    try {
      if (this.reader != null) {
        while (!(tmpLine = this.reader.readLine()).equals("done")) {
          mat = mat.append(tmpLine);
          mat = mat.append(System.lineSeparator());
        }
        return mat.toString();
      }
    } catch (IOException exception) {
      System.out.println(exception.toString());
    }

    System.out.println("ERROR: Failed to read client request.");
    return null;
  }

  public void handle(InputStream inFromClient, OutputStream outToClient) {
    this.reader = new BufferedReader(new InputStreamReader(inFromClient));
    this.writer = new PrintWriter(outToClient);
    String request = this.readRequest();
//    System.out.print(request);
//    System.out.println("--------------");
    if (request != null) {
      String problemId = String.valueOf(request.hashCode());
      // Check for existing solution
      ISolution solution = (ISolution) this.cacheManager.get(problemId);
//      solution = null;
      // If doesn't exist, solve it and return the solution (and save for the next time it is requested)
      if (solution == null) {
        solution = this.solver.solve(request);
        if (solution != null) {
          this.cacheManager.save(problemId, solution);
//          System.out.print(solution.toString());
        }
        this.writeResponse(solution);
      }
    }

    this.cleanUp();
  }

  private void writeResponse(ISolution solution) {
    if (solution != null && this.writer != null) {
      writer.print(solution.toString());
    }
    writer.println("done");
    writer.flush();
  }

  private void cleanUp() {
    if (this.reader != null) {
      try {
        this.reader.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (this.writer != null) {
      this.writer.close();
    }
  }
}