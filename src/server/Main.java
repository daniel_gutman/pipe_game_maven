import abstracts.State;
import algorithm.*;
import game.PipeGameBoard;
import game.PipeGameState;
import interfaces.*;
import clientHandlers.*;
import cache.*;
import servers.TcpServer;

import java.util.ArrayList;

/**
 * Created by maorshabo on 31/05/2018.
 */
public class Main {
  public static void main(String[] args) {
    startServer();
//    testSearcher(new BFS(), new PipeGame(1, 2));
//    testSearcher(new BestFirstSearch(), new PipeGame(3, 2));
    /*String[][] board = new String[3][2];
    board[0][0] = "s";
    board[0][1] = "╚";
    board[1][0] = "|";
    board[1][1] = "|";
    board[2][0] = "╚";
    board[2][1] = "g";
    testSearcher(new BestFirstSearch(), new PipeGame(board, new int[]{0,0}, new int[]{2, 1}));*/
//    testSearcher(new DFS(), new PipeGame(1, 2));
//    testSearcher(new HillClimbing(), new PipeGame(1, 2));
  }

  static void startServer() {
    try {
      IServer s = new TcpServer(1234);
      s.start(new StringClientHandler(new MemoryCache(), new BestFirstSearch()));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  static void testSearcher(ISearcher searcher, ISearchable searchable) {
    ISolution sol = searcher.search(searchable);
    if (sol != null) {
      int n = searcher.getNumberOfNodesEvaluated();
      ArrayList<State> path = sol.getSteps();
      if (path.size() > 0) {
        PipeGameState s = (PipeGameState) path.get(0);
        PipeGameBoard b = (PipeGameBoard) s.getState();
        System.out.println("-------- GOAL STATE --------");
        System.out.print(b.toString());
      }
      System.out.println(n + " nodes evaluated using " + searcher.getName());
      System.out.print(sol.toString());
    } else {
      System.out.println("No Solution");
    }
  }
}
