package servers;

import interfaces.IClientHandler;
import interfaces.IServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * Created by maorshabo on 31/05/2018.
 */

public class TcpServer implements IServer {
  private int port;
  private boolean isRunning;

  public TcpServer(int port) {
    this.port = port;
  }

  public void start(IClientHandler clientHandler) {
    this.isRunning = true;
    new Thread(() -> {
      try {
//        System.out.println("Server is running on port " + this.port);
        activate(clientHandler);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }).start();
  }

  private void activate(IClientHandler clientHandler) throws IOException {
    ServerSocket serverSocket = new ServerSocket(this.port);
    serverSocket.setSoTimeout(1000);
    while (this.isRunning) {
      try {
        Socket clientSocket = serverSocket.accept();
        String clientAddress = clientSocket.getInetAddress().getHostAddress();
//        System.out.println("\r\nNew connection from " + clientAddress);
        clientHandler.handle(clientSocket.getInputStream(), clientSocket.getOutputStream());
        clientSocket.close();
      } catch (SocketTimeoutException e) {
      }
    }
    serverSocket.close();
  }

  public void stop() {
    this.isRunning = false;
  }
}
