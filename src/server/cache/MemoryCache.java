package cache;

import interfaces.ICacheManager;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by maorshabo on 22/05/2018.
 */
public class MemoryCache implements ICacheManager {
  private Map<String, Object> cache;

  public MemoryCache() {
    this.cache = new HashMap<String, Object>();
  }

  @Override
  public Object get(String id) {
    return this.cache.get(id);
  }

  @Override
  public void save(String id, Object s) {
    // TODO: generate id given Object
    this.cache.put(id, s);
  }
}
