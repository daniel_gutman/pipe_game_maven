package cache;

import interfaces.*;

import java.io.*;

/**
 * Created by maorshabo on 21/05/2018.
 */
public class FileCache implements ICacheManager {

  private final String repository = "repository";
  private boolean isDirCreated = false;

  private void createDirIfNeeded() {
    // Check it the directory where we store the files was created, if not create it.
    File dir = new File(this.repository);
    if (!(dir.exists())) {
      isDirCreated = dir.mkdir();
      return;
    }
    isDirCreated = true;
  }


  //Gets the board that we would like to saveSolution, generates it's id and saves it to the relevant file.
  public void save(String id, Object obj) {
    try {
      if (!this.isDirCreated) {
        this.createDirIfNeeded();
      }
      File filePath = new File(this.repository, id);
      if (obj != null && !filePath.exists()) {
        boolean created = filePath.createNewFile();
        if (created) {
          ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                  new BufferedOutputStream(
                          new FileOutputStream(filePath)
                  ));
//          System.out.println(String.join(" ", "Saving the following solution:", obj.toString()));
          objectOutputStream.writeObject(obj);
          objectOutputStream.close();
        }
      }
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }


  //loads a problem or a solution according to the given param
  public Object get(String id) {
    try {
      File fileToLoad = new File(this.repository, id);
      ObjectInputStream objInput = new ObjectInputStream(new FileInputStream(fileToLoad));
      Object obj = objInput.readObject();
//      System.out.println(String.join(" ", "Loaded solution:\r\n", obj.toString()));
      return obj;
    } catch (IOException | ClassNotFoundException exception) {
//            System.out.println("This problem wasn't solved yet.");
    }
    return null;
  }

}
