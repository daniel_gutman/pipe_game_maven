
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class Client {
  PrintWriter out = null;
  private Socket socket;
  private String host = "127.0.0.1";
  private int port_number = 1234;

  public Client() {

  }

  public void send(String item) throws IOException, InterruptedException {
    this.socket = new Socket(this.host, this.port_number);

    out = new PrintWriter(socket.getOutputStream());

    out.println("sLL");
    out.println("FL|");
    out.println("7|g");
//    out.println("-----");
    /*out.println("s-LL");
    out.println("|-LL");
    out.println("|-|g");*/
    out.println("done");
    out.flush();

    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

    //this.socket.setSoTimeout(600);
    try {
      String serverMessage = inFromServer.readLine(); // it will timeout in 200 ms
      System.out.println(serverMessage);
    } catch (SocketTimeoutException e) {
      System.out.println(e.getStackTrace());
    }
  }

  public void startGame(String start_game) {
    try {
      send(start_game);

    } catch (UnknownHostException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}