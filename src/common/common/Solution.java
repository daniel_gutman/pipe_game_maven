package common;

import abstracts.State;

import java.util.*;

import abstracts.Step;
import interfaces.ISolution;

import java.io.Serializable;
import java.lang.reflect.Array;

/**
 * Created by maorshabo on 28/06/2018.
 */
public class Solution implements ISolution, Serializable {
  ArrayList<State> steps;

  public Solution() {
    this.steps = new ArrayList<>();
  }

  public Solution(ArrayList<State> steps) {
    this.steps = steps;
    Collections.reverse(this.steps);
  }

  public Solution(State goalState) {
    ArrayList<State> steps = new ArrayList<>();
    State currentState = goalState;

    while (currentState.getCameFrom() != null) {
      steps.add(currentState);
      currentState = currentState.getCameFrom();
    }

    this.steps = steps;
    Collections.reverse(this.steps);
  }

  @Override
  public ArrayList<State> getSteps() {
    return this.steps;
  }

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder();
    if (this.steps.size() == 0) {
      return "No steps needed, the puzzle is already solved";
    }
    for (State state : this.steps) {
      result.append(state.getStep().toString()).append("\n");
    }
    return result.toString();
  }
}
