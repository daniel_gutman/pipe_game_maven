package abstracts;

import java.io.Serializable;

/**
 * Created by maorshabo on 20/05/2018.
 */
public abstract class State<T> implements Serializable {
  protected T state;
  protected Step<T> step;
  private State cameFrom; // the state we came from to this state
  private double _cost;
  protected int _flowCount;

  public State(T state) {
    this.state = state;
  }

  public T getState() {
    return state;
  }

  public void setCameFrom(State cameFrom) {
    this.cameFrom = cameFrom;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!State.class.isAssignableFrom(obj.getClass())) {
      return false;
    }
    final State other = (State) obj;
//    return this.state.equals(other.state);
    return this.step.equals(other.step);
  }

  public State getCameFrom() {
    return cameFrom;
  }

  public double getCost() {
    return _cost;
  }

  public void setCost(double cost) {
    this._cost = cost;
  }

  public int getFlowCount() {
    return _flowCount;
  }

  public Step<T> getStep() {
    return step;
  }

  public void setStep(Step<T> step) {
    this.step = step;
  }

  public abstract double generateCost();
}
