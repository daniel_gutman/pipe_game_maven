package abstracts;

import interfaces.ISearchable;
import interfaces.ISearcher;
import interfaces.ISolution;

import java.util.PriorityQueue;

/**
 * Created by maorshabo on 11/06/2018.
 */
public abstract class QueueBasedSearcher implements ISearcher {
  protected PriorityQueue<State> openList;
  private int evaluatedNodes;

  public QueueBasedSearcher() {
    openList = new PriorityQueue<State>();
    evaluatedNodes = 0;
  }

  protected State popOpenList() {
    evaluatedNodes++;
    return openList.poll();
  }

  @Override
  public abstract ISolution search(ISearchable s);

  @Override
  public int getNumberOfNodesEvaluated() {
    return evaluatedNodes;
  }

  @Override
  public void reset() {
    this.evaluatedNodes = 0;
  }
}
