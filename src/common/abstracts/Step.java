package abstracts;

import java.io.Serializable;

/**
 * Created by maorshabo on 18/08/2018.
 */
public abstract class Step<T> implements Serializable {
  protected T step;

  public T getStep() {
    return step;
  }

  public abstract void setStep(T step);

  @Override
  public abstract String toString();
}