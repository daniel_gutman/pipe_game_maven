package algorithm;

import abstracts.QueueBasedSearcher;
import abstracts.State;
import common.Solution;
import interfaces.*;//utils.interfaces.ISearchable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 * Created by maorshabo on 20/05/2018.
 */
public class BestFirstSearch extends QueueBasedSearcher {

  public BestFirstSearch() {
    super();
  }

  @Override
  public ISolution search(ISearchable s) {
    this.openList = new PriorityQueue<State>(1, s.getComparator());
    this.openList.add(s.getInitialState());
    HashSet<State> closedSet = new HashSet<State>();

    while (this.openList.size() > 0) {
      State n = popOpenList();
      closedSet.add(n);

      if (s.isGoal(n)) {
        return this.backTrace(n);
      }
      ArrayList<State> successors = s.getAllPossibleStates(n);
      for (State state : successors) {
        if (!closedSet.contains(state) && !this.openList.contains(state)) {
          state.setCameFrom(n);
          this.openList.add(state);
        } else {
          // Otherwise, if this new path is better than previous one
          // i. If it is not in OPEN add it to OPEN.
          // ii. Otherwise, adjust its priority in OPEN
          if (this.openList.contains(state)) {
            double newCost = state.generateCost() + 1;
            for (State stateInOpen : this.openList) {
              if (stateInOpen.equals(state)) {
                if (newCost < stateInOpen.generateCost()) {
//                  stateInOpen.setFlowCount(newCost);
                  stateInOpen.setCameFrom(state);
                }
              }
            }
          }
        }
      }
    }
    return null;
  }

  private ISolution backTrace(State goalState) {
    ArrayList<State> steps = new ArrayList<>();
    State currentState = goalState;

    while (currentState.getCameFrom() != null) {
      steps.add(currentState);
      currentState = currentState.getCameFrom();
    }

    return new Solution(steps);
  }

  @Override
  public String getName() {
    return "BestFirstSearch";
  }
}
