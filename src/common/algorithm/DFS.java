package algorithm;

import abstracts.State;
import common.Solution;
import interfaces.ISearchable;
import interfaces.ISearcher;
import interfaces.ISolution;

import java.util.Stack;
import java.util.ArrayList;


/**
 * Created by maorshabo on 21/05/2018.
 */
public class DFS implements ISearcher {
  private int _numberOfNodesEvaluated;

  @Override
  public ISolution search(ISearchable searchable) {
    // Array list with all the states we visited at
    ArrayList<State> visitedStates = new ArrayList<>();
    // Stack to manage which of the state we need to work on
    Stack<State> stack = new Stack<>();
    // States that will be part of our solution
    State rootSolution = searchable.getInitialState();
    stack.push(rootSolution);
    // As long as we have states that we need to work on
    State currentState;
    while (!stack.empty()) {
      currentState = stack.pop();
      visitedStates.add(currentState);

      if (searchable.isGoal(currentState)) {
        // Found the goal, return the back track from solution
        return new Solution(currentState);
      }
      ArrayList<State> possibleStates = searchable.getAllPossibleStates(currentState);

      for (State state : possibleStates) {
        if (!visitedStates.contains(state) && !stack.contains(state)) {
          state.setCameFrom(currentState);
          visitedStates.add(state);
          stack.push(state);
          this._numberOfNodesEvaluated++;
        }

      }
    }
    return null;
  }

  public DFS() {
    _numberOfNodesEvaluated = 1;
  }

  @Override
  public int getNumberOfNodesEvaluated() {
    return _numberOfNodesEvaluated;
  }

  @Override
  public void reset() {
    this._numberOfNodesEvaluated = 0;
  }

  public String getName() {
    return "DFS";
  }
}