package algorithm;

import abstracts.State;
import common.Solution;
import interfaces.*;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by maorshabo on 21/05/2018.
 */
public class BFS implements ISearcher {
  private int evaluatedNodes;

  public BFS() {
    this.evaluatedNodes = 0;
  }

  @Override
  public ISolution search(ISearchable s) {
    this.evaluatedNodes = 0;
    //Define Array list of all the visited States
    ArrayList<State> visitedStates = new ArrayList<>();
    //Define the Queue for the discover States
    LinkedList<State> queue = new LinkedList<State>();
    //The first state -Initial state
    State rootSolution = s.getInitialState();
    //Add the first State to the queue
    queue.add(rootSolution);
    //Run the algorithm while the queue is not empty
    while (!queue.isEmpty()) {
      State currentState;
      //get the first one in queue
      currentState = queue.poll();
      visitedStates.add(currentState);
//      s.setCurrentState(currentState);
      //check if the current state is the goal
      if (s.isGoal(currentState)) {
        //return the backTrace of the current State
        return new Solution(currentState);
      }
      //Create array list to the possible States
      ArrayList<State> possibleStates = s.getAllPossibleStates(currentState);
      for (State state : possibleStates) {
        //to have all the states that I came from them.
        state.setCameFrom(currentState);
        //check if the possible state is already in the queue and visited state
        if (!queue.contains(state) && !visitedStates.contains(state)) {
          queue.add(state);
          visitedStates.add(state);
          this.evaluatedNodes++;
        }
      }
    }
    return null;
  }

  @Override
  public int getNumberOfNodesEvaluated() {
    return this.evaluatedNodes;
  }

  @Override
  public String getName() {
    return "BFS";
  }

  @Override
  public void reset() {
    this.evaluatedNodes = 0;
  }
}
