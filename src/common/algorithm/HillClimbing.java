package algorithm;

import abstracts.State;
import common.Solution;
import interfaces.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by maorshabo on 21/05/2018.
 */
public class HillClimbing implements ISearcher {
  private int _numberOfNodesEvaluated;
  @Override
  public ISolution search(ISearchable s) {
    this._numberOfNodesEvaluated = 0;
    State currentState = s.getInitialState();
    State bestNeighborState = null;

    while (true) {
      List<State> neighbors = new ArrayList<>(s.getAllPossibleStates(currentState));
      for (State neighbor : neighbors) {
        neighbor.setCameFrom(currentState);
      }
      double grade = Double.MAX_VALUE;
      if (Math.random() < 0.7) {
        for (State neighbor : neighbors) {
          double g = neighbor.generateCost() * -1;
          if (g < grade) {
            bestNeighborState = neighbor;
            grade = g;
          }
        }

        if (bestNeighborState == null) {
          bestNeighborState = currentState;
        }

        if (s.isGoal(currentState)) {
          return new Solution(currentState);
        }

        if (currentState.generateCost() > bestNeighborState.generateCost()) {
          currentState = bestNeighborState;
          this._numberOfNodesEvaluated++;
        }

      } else {
        if (neighbors.isEmpty()) {
          break;
        }
        Random r = new Random();
        int randomIndex = r.nextInt(neighbors.size());
        currentState = neighbors.get(randomIndex);
        this._numberOfNodesEvaluated++;
      }
    }
    return null;
  }

  @Override
  public int getNumberOfNodesEvaluated() {
    return this._numberOfNodesEvaluated;
  }

  @Override
  public String getName() {
    return "HillClimbing";
  }

  @Override
  public void reset() {
    this._numberOfNodesEvaluated = 0;
  }
}
