package interfaces;

public interface ICacheManager {

  void save(String id, Object solution);
  Object get(String id);
}