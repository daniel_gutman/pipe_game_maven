package interfaces;

//import pipegamegame.server.abstracts.ClientHandler;
//import com.
import java.io.IOException;

/**
 * Created by maorshabo on 22/05/2018.
 */
public interface IServer {
  void start(IClientHandler ch) throws Exception;
  void stop() throws IOException;
}
