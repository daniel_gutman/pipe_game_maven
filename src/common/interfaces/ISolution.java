package interfaces;

import abstracts.State;

import java.util.ArrayList;
import java.util.Queue;

/**
 * Created by maorshabo on 11/06/2018.
 */
public interface ISolution {
  ArrayList<State> getSteps();
}


