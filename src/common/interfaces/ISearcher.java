package interfaces;

/**
 * Created by maorshabo on 20/05/2018.
 */
public interface ISearcher {
  // the search method
  ISolution search(ISearchable s);
// get how many nodes were evaluated by the algorithm
  int getNumberOfNodesEvaluated();
  void reset();
  String getName();
}