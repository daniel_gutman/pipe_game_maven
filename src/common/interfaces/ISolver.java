package interfaces;

/**
 * Created by maorshabo on 22/05/2018.
 */
public interface ISolver {
  ISolution solve(String problem);
}
