package interfaces;
import abstracts.State;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by maorshabo on 20/05/2018.
 */
public interface ISearchable<T> {
  State getInitialState();
  State getGoalState();
  boolean isGoal(State s);
  ArrayList<State> getAllPossibleStates(State s);
  Comparator<State> getComparator();
}
