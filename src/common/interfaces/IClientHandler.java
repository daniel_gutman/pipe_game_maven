package interfaces;

import java.io.InputStream;
import java.io.OutputStream;

public interface IClientHandler {
    void handle(InputStream inFromClient, OutputStream outToClient);
}
